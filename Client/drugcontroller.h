#ifndef DRUGCONTROLLER_H
#define DRUGCONTROLLER_H

#define  NOMINMAX

#include <windows.h>
#include <QString>
#include <QVector>
#include <QRegularExpression>
#include "../Common/requesttype.h"
#include "../Common/drug.h"
#include "../Common/rowofbrowser.h"
#include "../Common/io.h"

class DrugController
{
public:
    DrugController();
    ~DrugController();

    /*!
     * Получить количество записей.
     *\return Количество записей.
     */
    int count() const;

    /*!
     * Добавить запись в базу данных
     *\param[in|out] rec Запись лекарства.
     *\return Позиция записи в соотвествии с порядком сортировки запись с присвоенным ей id.
     */
    int append(Drug& rec) const;

    /*!
     * Удалить из базы данных запись с заданным идентификатором.
     *\param[in] id Идентификатор записи.
     */
    void remove(uint id) const;

    /*!
     * Изменить запись в базе данных.
     *\param[in] id Идентификатор редактируемой записи.
     *\param[in|out] name Название редактируемого поля.
     *\param[in|out] value Новое значение редактируемого поля (в текстовом формате).
     *\return Новая позиция записи в соответствии с порядком сортировки.
     */
    int update(uint id, const QString& name, const QString& value) const;

    /*!
     * Возвращает запись (только для чтения) по заданному идентификатору.
     *\param [in] id Идентификатор записи.
     */
    const Drug record(uint id) const;

    /*!
     * Возвращает вектор записей, которые должны отображается в браузере с учетом сортировки записей.
     *\return Вектор записей, которые должны отображается в браузере с учетом сортировки записей, где rowOfBrowser — структура, соответствующая строке браузера (поля  структуры  совпадают с колонками браузера, также структура содержит идентификатор записи).
     */
    const QVector<RowOfBrowser> records() const;

    /*!
     * Сохранить данные на сервере.
     */
    bool save() const;

    /*!
     * Соединиться с сервером.
     *\return Признак успешности соединения.
     */
    bool runServer(const QString& serverPath, const QString& pipeName);

    /*!
     * Отсоединиться от сервера.
     *\return Признак успешности отсоединения.
     */
    bool stopServer() const;

    /*!
     * Проверить соединение с сервером.
     */
    bool checkServer() const;
    
private:
    // Дескриптор канала.
    HANDLE hPipe;
};

#endif // DRUGCONTROLLER_H
