#include "drugcontroller.h"

DrugController::DrugController()
{
}

DrugController::~DrugController()
{
}

int DrugController::count() const
{
    DWORD bytes;
    int size;
    RequestType request = RequestType::COUNT;

    WriteFile(
        hPipe,
        (void*) &request,
        sizeof(int),
        &bytes,
        NULL);

    ReadFile(
        hPipe,
        (void*) &size,
        sizeof(int),
        &bytes,
        NULL);

    return size;
}

int DrugController::append(Drug& rec) const
{
    DWORD bytes;
    RequestType request = RequestType::APPEND;

    WriteFile(hPipe, (void*) &request, sizeof(int), &bytes, NULL);
    writeDrug(hPipe, rec);

    int pos;
    ReadFile(hPipe, (void*) &pos, sizeof(int), &bytes, NULL);
    uint id;
    ReadFile(hPipe, (void*) &id, sizeof(uint), &bytes, NULL);

    rec.setId(id);

    return pos;
}

void DrugController::remove(uint id) const
{
    DWORD bytes;
    RequestType request = RequestType::REMOVE;

    WriteFile(hPipe, (void*) &request, sizeof(int), &bytes, NULL);
    WriteFile(hPipe, (void*) &id, sizeof(uint), &bytes, NULL);
}

int DrugController::update(uint id, const QString& name, const QString& value) const
{
    DWORD bytes;
    RequestType request = RequestType::UPDATE;

    WriteFile(hPipe, (void*) &request, sizeof(int), &bytes, NULL);
    WriteFile(hPipe, (void*) &id, sizeof(uint), &bytes, NULL);
    writeString(hPipe, name);
    writeString(hPipe, value);

    int pos;
    ReadFile(hPipe, (void*) &pos, sizeof(int), &bytes, NULL);

    return pos;
}

const Drug DrugController::record(uint id) const
{
    DWORD bytes;
    RequestType request = RequestType::RECORD;

    WriteFile(hPipe, (void*) &request, sizeof(int), &bytes, NULL);
    WriteFile(hPipe, (void*) &id, sizeof(uint), &bytes, NULL);

    return readDrug(hPipe);
}

const QVector<RowOfBrowser> DrugController::records() const
{
    DWORD bytes;

    RequestType request = RequestType::RECORDS;
    WriteFile(hPipe, (void*) &request, sizeof(int), &bytes, NULL);

    int count;
    ReadFile(hPipe, (void*) &count, sizeof(int), &bytes, NULL);

    RowOfBrowser* rob = new RowOfBrowser[count];

    QVector<RowOfBrowser> drugs;
    for (int i = 0; i < count; ++i)
    {
        rob[i] = readRowOfBrowser(hPipe);
        drugs << rob[i];
    }

    delete[] rob;

    return drugs;
}

bool DrugController::save() const
{
    DWORD bytes;
    RequestType request = RequestType::SAVE;
    return WriteFile(hPipe, (void*) &request, sizeof(int), &bytes, NULL);
}

bool DrugController::runServer(const QString& serverPath, const QString& pipeName)
{
    STARTUPINFO startupInfo;
    PROCESS_INFORMATION processInfo;
    int startupInfoSize = sizeof(startupInfo);
    ZeroMemory(&startupInfo, startupInfoSize);
    startupInfo.cb = startupInfoSize;
    bool result = false;

    QString serverName = serverPath.section(QRegularExpression("[\\|\\/]"), -1);

    if (CreateProcess(
        (const wchar_t*) serverPath.utf16(),
        (wchar_t*) serverName.utf16(),
        NULL,
        NULL,
        FALSE,
        0,
        NULL,
        NULL,
        &startupInfo,
        &processInfo))
    {
        while (!WaitNamedPipe((const wchar_t*) pipeName.utf16(), NMPWAIT_USE_DEFAULT_WAIT));

        hPipe = CreateFile(
            (const wchar_t*) pipeName.utf16(),
            GENERIC_READ | GENERIC_WRITE,
            0,
            NULL,
            OPEN_EXISTING,
            0,
            NULL);

        if (hPipe != INVALID_HANDLE_VALUE)
        {
            DWORD mode = PIPE_TYPE_BYTE | PIPE_READMODE_BYTE | PIPE_WAIT;
            SetNamedPipeHandleState(hPipe, &mode, NULL, NULL);
            result = true;
        }
    }

    return result;
}

bool DrugController::stopServer() const
{
    DWORD bytes;
    RequestType request = RequestType::EXIT;
    return WriteFile(hPipe, (void*) &request, sizeof(int), &bytes, NULL);
}

bool DrugController::checkServer() const
{
    RequestType request = RequestType::CHECK;
    DWORD bytes;
    int response = 0;

    WriteFile(hPipe, (void*) &request, sizeof(int), &bytes, NULL);
    ReadFile(hPipe, (void*) &response, sizeof(int), &bytes, NULL);

    if (response)
    {
        return true;
    }

    return false;
}
