#include "client.h"
#include <QtWidgets/QApplication>
#include <QTranslator>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);

    QTranslator *qt_translator = new QTranslator;
    if (qt_translator->load("Resources/qt_ru.qm"))
    {
        a.installTranslator(qt_translator);
    }

    Client w;
    w.show();
    return a.exec();
}
