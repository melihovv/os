#include "packvalidator.h"

PackValidator::PackValidator(QObject* parent)
    : QValidator(parent)
{
}

PackValidator::~PackValidator()
{
}

QValidator::State PackValidator::validate(QString& input, int& pos) const
{
    QRegExp letter("[A-Za-zА-Яа-я]");
    QRegExp number("[0-9]+");

    if (input.length() <= 15 && input.contains(number) && input.contains(letter))
    {
        // В строке есть хотя бы одна буква и цифра.
        return QValidator::Acceptable;
    }
    else if (input.length() >= 15 && (!input.contains(letter) || !input.contains(number)))
    {
        // В строке нет буквы или цифры.
        return QValidator::Invalid;
    }
    else
    {
        return QValidator::Intermediate;
    }
}
