#ifndef CLIENT_H
#define CLIENT_H

#include <ctime>
#include <QtWidgets/QMainWindow>
#include <QMessageBox>
#include <QFileDialog>
#include <QCloseEvent>
#include <QSettings>
#include <QTimer>
#include "ui_client.h"
#include "../Common/drug.h"
#include "../Common/rowofbrowser.h"
#include "drugcontroller.h"
#include "namevalidator.h"
#include "packvalidator.h"

class Client : public QMainWindow
{
    Q_OBJECT

public:
    enum class CategoryIndex
    {
        PYRETIC,
        ANTIBIOTIC,
        ANALGESIC,
        NUTRACEUTICALS
    };

    Client(QWidget* parent = 0);
    ~Client();

    /*!
     * Подключить все необходимые валидаторы.
     */
    void setValidation();

    /*!
     * Соединить все сигналы со слотами.
     */
    void setConnection() const;

    /*!
     * Деактивировать/активировать виджеты при пустом/непустом браузере записей.
     *\param[in] flag Деактивировать или активировать виджеты.
     */
    void setActivity(bool flag) const;

    /*!
     * Установить значения виджетов "по умолчанию".
     */
    void setStandartWidgets() const;

    /*!
     * Вывести запись во все виджеты.
     */
    void showRecord();

    /*!
     * Вывести запись в виджеты редактирования, кроме браузера.
     */
    void showRecordWithoutBrowser();

    /*!
     * Вывести запись в браузер.
     */
    void showRecordInBrowser();

    /*!
    * Открыть бд.
    */
    void loadDb();

private slots:
    /*!
     * Деактивировать поля "Требуется рецепт" и "Дата сертификации", если выбрано "БАД" из перечня.
     *\param[in] currentRow Индекс выбранной категории.
     */
    void disableIfBAD(int currentRow) const;

    /*!
     * Показать запись, выбранную в браузере.
     *\param[in] currentRow Индекс текущей записи в браузере.
     */
    void showRecordChoosedInBrowser(int currentRow);

    /*!
     * Создать запись "по умолчанию" или запись на основе данной записи.
     *\param[in] another Если значение "по умолчанию", то создается запись по умолчанию, иначе - создается запись на основе данной записи.
     */
    void createRecord(Drug& another = Drug());

    /*!
     * Создать десять записей.
     */
    void createRecords();

    /*!
     * Удалить текущую запись.
     */
    void deleteCurRec();

    /*!
     * Сохранить данные в массив при потере виджетом фокуса.
     *\param[in] last Указатель на виджет, потерявший фокус.
     *\param[in] now Указатель на виджет, находящийся в фокусе.
     */
    void saveData(QWidget* last, QWidget* now);

    /*!
    * Обработать закрытие окна.
    *\param[in] e Событие закрытия окна.
    */
    void closeEvent(QCloseEvent* e);

    /*!
     * Проверить соединение с сервером.
     */
    void checkServer();

private:
    Ui::ClientClass ui;
    // Максимальное количество записей.
    static const int MAX_RECORDS = 100;
    // Максимальное количество записей.
    QValidator* validators[2];
    // Модель записей лекарств.
    DrugController controller;
    // Строки браузера.
    QVector<RowOfBrowser> rowsOfBrowser;
    // Позиция текущей записи в соотвествии с порядком сортировки.
    int currentPosition;
    // Идентификатор текущей записи.
    int currentId; 
    // Таймер для проверки сервера.
    QTimer* checkServerTimer;
};

#endif // CLIENT_H
