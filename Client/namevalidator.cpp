#include "namevalidator.h"

NameValidator::NameValidator(QObject* parent)
    : QValidator(parent)
{
}

NameValidator::~NameValidator()
{
}

QValidator::State NameValidator::validate(QString& input, int& pos) const
{
    if (input[0].isUpper() &&
        input.length() <= 40 &&
        input.length() > 0)
    {
        // Строка начинается с большой буквы.
        return QValidator::Acceptable;
    }
    else if (!input[0].isUpper() &&
        input.length() < 40)
    {
        // Строка начинается не с большой буквы, но еще есть место для ввода.
        return QValidator::Intermediate;
    }
    else
    {
        return QValidator::Invalid;
    }
}
