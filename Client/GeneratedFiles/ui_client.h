/********************************************************************************
** Form generated from reading UI file 'client.ui'
**
** Created by: Qt User Interface Compiler version 5.4.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_CLIENT_H
#define UI_CLIENT_H

#include <QtCore/QDate>
#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QCheckBox>
#include <QtWidgets/QDateEdit>
#include <QtWidgets/QGroupBox>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QListWidget>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QSpinBox>
#include <QtWidgets/QTextEdit>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_ClientClass
{
public:
    QAction *navNew;
    QAction *navOpen;
    QAction *navSave;
    QAction *navSaveAs;
    QAction *navExit;
    QWidget *centralWidget;
    QHBoxLayout *horizontalLayout_12;
    QVBoxLayout *verticalLayout_2;
    QHBoxLayout *horizontalLayout_11;
    QVBoxLayout *verticalLayout;
    QLabel *title;
    QListWidget *listRecords;
    QGroupBox *infAboutDrug;
    QWidget *layoutWidget;
    QHBoxLayout *horizontalLayout_2;
    QLabel *label;
    QLineEdit *lineName;
    QWidget *layoutWidget1;
    QHBoxLayout *horizontalLayout;
    QLabel *label_5;
    QSpinBox *spinBoxCost;
    QSpacerItem *horizontalSpacer_4;
    QSpacerItem *horizontalSpacer_3;
    QWidget *layoutWidget2;
    QHBoxLayout *horizontalLayout_8;
    QLabel *label_2;
    QListWidget *listCategory;
    QWidget *layoutWidget3;
    QHBoxLayout *horizontalLayout_3;
    QLabel *label_6;
    QLineEdit *lineContraindication;
    QWidget *layoutWidget4;
    QHBoxLayout *horizontalLayout_5;
    QLabel *label_8;
    QDateEdit *dateCertification;
    QSpacerItem *horizontalSpacer_5;
    QWidget *layoutWidget5;
    QHBoxLayout *horizontalLayout_7;
    QLabel *label_3;
    QTextEdit *textEditPrescription;
    QWidget *layoutWidget6;
    QHBoxLayout *horizontalLayout_9;
    QLabel *label_7;
    QCheckBox *checkIsRecipeNeed;
    QSpacerItem *horizontalSpacer_6;
    QWidget *layoutWidget7;
    QHBoxLayout *horizontalLayout_10;
    QHBoxLayout *horizontalLayout_6;
    QLabel *label_4;
    QLineEdit *linePack;
    QHBoxLayout *horizontalLayout_4;
    QSpacerItem *horizontalSpacer;
    QPushButton *btnCreate;
    QPushButton *btnDelete;
    QPushButton *btnFill;

    void setupUi(QMainWindow *ClientClass)
    {
        if (ClientClass->objectName().isEmpty())
            ClientClass->setObjectName(QStringLiteral("ClientClass"));
        ClientClass->resize(845, 491);
        ClientClass->setMinimumSize(QSize(845, 491));
        navNew = new QAction(ClientClass);
        navNew->setObjectName(QStringLiteral("navNew"));
        navOpen = new QAction(ClientClass);
        navOpen->setObjectName(QStringLiteral("navOpen"));
        navSave = new QAction(ClientClass);
        navSave->setObjectName(QStringLiteral("navSave"));
        navSaveAs = new QAction(ClientClass);
        navSaveAs->setObjectName(QStringLiteral("navSaveAs"));
        navExit = new QAction(ClientClass);
        navExit->setObjectName(QStringLiteral("navExit"));
        centralWidget = new QWidget(ClientClass);
        centralWidget->setObjectName(QStringLiteral("centralWidget"));
        horizontalLayout_12 = new QHBoxLayout(centralWidget);
        horizontalLayout_12->setSpacing(6);
        horizontalLayout_12->setContentsMargins(11, 11, 11, 11);
        horizontalLayout_12->setObjectName(QStringLiteral("horizontalLayout_12"));
        verticalLayout_2 = new QVBoxLayout();
        verticalLayout_2->setSpacing(6);
        verticalLayout_2->setObjectName(QStringLiteral("verticalLayout_2"));
        horizontalLayout_11 = new QHBoxLayout();
        horizontalLayout_11->setSpacing(6);
        horizontalLayout_11->setObjectName(QStringLiteral("horizontalLayout_11"));
        verticalLayout = new QVBoxLayout();
        verticalLayout->setSpacing(6);
        verticalLayout->setObjectName(QStringLiteral("verticalLayout"));
        title = new QLabel(centralWidget);
        title->setObjectName(QStringLiteral("title"));

        verticalLayout->addWidget(title);

        listRecords = new QListWidget(centralWidget);
        listRecords->setObjectName(QStringLiteral("listRecords"));
        listRecords->setMinimumSize(QSize(481, 383));

        verticalLayout->addWidget(listRecords);


        horizontalLayout_11->addLayout(verticalLayout);

        infAboutDrug = new QGroupBox(centralWidget);
        infAboutDrug->setObjectName(QStringLiteral("infAboutDrug"));
        infAboutDrug->setMinimumSize(QSize(311, 383));
        infAboutDrug->setFlat(false);
        infAboutDrug->setCheckable(false);
        layoutWidget = new QWidget(infAboutDrug);
        layoutWidget->setObjectName(QStringLiteral("layoutWidget"));
        layoutWidget->setGeometry(QRect(10, 23, 291, 22));
        horizontalLayout_2 = new QHBoxLayout(layoutWidget);
        horizontalLayout_2->setSpacing(6);
        horizontalLayout_2->setContentsMargins(11, 11, 11, 11);
        horizontalLayout_2->setObjectName(QStringLiteral("horizontalLayout_2"));
        horizontalLayout_2->setContentsMargins(0, 0, 0, 0);
        label = new QLabel(layoutWidget);
        label->setObjectName(QStringLiteral("label"));
        label->setMinimumSize(QSize(110, 0));

        horizontalLayout_2->addWidget(label);

        lineName = new QLineEdit(layoutWidget);
        lineName->setObjectName(QStringLiteral("lineName"));
        lineName->setMinimumSize(QSize(0, 0));

        horizontalLayout_2->addWidget(lineName);

        layoutWidget1 = new QWidget(infAboutDrug);
        layoutWidget1->setObjectName(QStringLiteral("layoutWidget1"));
        layoutWidget1->setGeometry(QRect(10, 290, 291, 22));
        horizontalLayout = new QHBoxLayout(layoutWidget1);
        horizontalLayout->setSpacing(6);
        horizontalLayout->setContentsMargins(11, 11, 11, 11);
        horizontalLayout->setObjectName(QStringLiteral("horizontalLayout"));
        horizontalLayout->setContentsMargins(0, 0, 0, 0);
        label_5 = new QLabel(layoutWidget1);
        label_5->setObjectName(QStringLiteral("label_5"));
        label_5->setMinimumSize(QSize(110, 0));

        horizontalLayout->addWidget(label_5);

        spinBoxCost = new QSpinBox(layoutWidget1);
        spinBoxCost->setObjectName(QStringLiteral("spinBoxCost"));
        spinBoxCost->setFrame(true);
        spinBoxCost->setMaximum(1000000);
        spinBoxCost->setValue(0);

        horizontalLayout->addWidget(spinBoxCost);

        horizontalSpacer_4 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout->addItem(horizontalSpacer_4);

        horizontalSpacer_3 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout->addItem(horizontalSpacer_3);

        layoutWidget2 = new QWidget(infAboutDrug);
        layoutWidget2->setObjectName(QStringLiteral("layoutWidget2"));
        layoutWidget2->setGeometry(QRect(10, 51, 291, 81));
        horizontalLayout_8 = new QHBoxLayout(layoutWidget2);
        horizontalLayout_8->setSpacing(6);
        horizontalLayout_8->setContentsMargins(11, 11, 11, 11);
        horizontalLayout_8->setObjectName(QStringLiteral("horizontalLayout_8"));
        horizontalLayout_8->setContentsMargins(0, 0, 0, 0);
        label_2 = new QLabel(layoutWidget2);
        label_2->setObjectName(QStringLiteral("label_2"));
        label_2->setMinimumSize(QSize(110, 0));

        horizontalLayout_8->addWidget(label_2);

        listCategory = new QListWidget(layoutWidget2);
        new QListWidgetItem(listCategory);
        new QListWidgetItem(listCategory);
        new QListWidgetItem(listCategory);
        new QListWidgetItem(listCategory);
        listCategory->setObjectName(QStringLiteral("listCategory"));
        listCategory->setMinimumSize(QSize(0, 0));

        horizontalLayout_8->addWidget(listCategory);

        layoutWidget3 = new QWidget(infAboutDrug);
        layoutWidget3->setObjectName(QStringLiteral("layoutWidget3"));
        layoutWidget3->setGeometry(QRect(10, 230, 291, 22));
        horizontalLayout_3 = new QHBoxLayout(layoutWidget3);
        horizontalLayout_3->setSpacing(6);
        horizontalLayout_3->setContentsMargins(11, 11, 11, 11);
        horizontalLayout_3->setObjectName(QStringLiteral("horizontalLayout_3"));
        horizontalLayout_3->setContentsMargins(0, 0, 0, 0);
        label_6 = new QLabel(layoutWidget3);
        label_6->setObjectName(QStringLiteral("label_6"));
        label_6->setMinimumSize(QSize(110, 0));

        horizontalLayout_3->addWidget(label_6);

        lineContraindication = new QLineEdit(layoutWidget3);
        lineContraindication->setObjectName(QStringLiteral("lineContraindication"));
        lineContraindication->setMinimumSize(QSize(0, 0));
        lineContraindication->setMaxLength(200);

        horizontalLayout_3->addWidget(lineContraindication);

        layoutWidget4 = new QWidget(infAboutDrug);
        layoutWidget4->setObjectName(QStringLiteral("layoutWidget4"));
        layoutWidget4->setGeometry(QRect(10, 350, 291, 22));
        horizontalLayout_5 = new QHBoxLayout(layoutWidget4);
        horizontalLayout_5->setSpacing(6);
        horizontalLayout_5->setContentsMargins(11, 11, 11, 11);
        horizontalLayout_5->setObjectName(QStringLiteral("horizontalLayout_5"));
        horizontalLayout_5->setContentsMargins(0, 0, 0, 0);
        label_8 = new QLabel(layoutWidget4);
        label_8->setObjectName(QStringLiteral("label_8"));
        label_8->setMinimumSize(QSize(110, 0));

        horizontalLayout_5->addWidget(label_8);

        dateCertification = new QDateEdit(layoutWidget4);
        dateCertification->setObjectName(QStringLiteral("dateCertification"));
        dateCertification->setDateTime(QDateTime(QDate(2000, 1, 1), QTime(0, 0, 0)));
        dateCertification->setMaximumDate(QDate(2014, 10, 10));
        dateCertification->setMinimumDate(QDate(1990, 1, 1));
        dateCertification->setDate(QDate(2000, 1, 1));

        horizontalLayout_5->addWidget(dateCertification);

        horizontalSpacer_5 = new QSpacerItem(78, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_5->addItem(horizontalSpacer_5);

        layoutWidget5 = new QWidget(infAboutDrug);
        layoutWidget5->setObjectName(QStringLiteral("layoutWidget5"));
        layoutWidget5->setGeometry(QRect(10, 140, 291, 81));
        horizontalLayout_7 = new QHBoxLayout(layoutWidget5);
        horizontalLayout_7->setSpacing(6);
        horizontalLayout_7->setContentsMargins(11, 11, 11, 11);
        horizontalLayout_7->setObjectName(QStringLiteral("horizontalLayout_7"));
        horizontalLayout_7->setContentsMargins(0, 0, 0, 0);
        label_3 = new QLabel(layoutWidget5);
        label_3->setObjectName(QStringLiteral("label_3"));
        label_3->setMinimumSize(QSize(110, 0));

        horizontalLayout_7->addWidget(label_3);

        textEditPrescription = new QTextEdit(layoutWidget5);
        textEditPrescription->setObjectName(QStringLiteral("textEditPrescription"));
        textEditPrescription->setMinimumSize(QSize(0, 0));

        horizontalLayout_7->addWidget(textEditPrescription);

        layoutWidget6 = new QWidget(infAboutDrug);
        layoutWidget6->setObjectName(QStringLiteral("layoutWidget6"));
        layoutWidget6->setGeometry(QRect(10, 320, 291, 22));
        horizontalLayout_9 = new QHBoxLayout(layoutWidget6);
        horizontalLayout_9->setSpacing(6);
        horizontalLayout_9->setContentsMargins(11, 11, 11, 11);
        horizontalLayout_9->setObjectName(QStringLiteral("horizontalLayout_9"));
        horizontalLayout_9->setContentsMargins(0, 0, 0, 0);
        label_7 = new QLabel(layoutWidget6);
        label_7->setObjectName(QStringLiteral("label_7"));
        label_7->setMinimumSize(QSize(110, 0));

        horizontalLayout_9->addWidget(label_7);

        checkIsRecipeNeed = new QCheckBox(layoutWidget6);
        checkIsRecipeNeed->setObjectName(QStringLiteral("checkIsRecipeNeed"));
        checkIsRecipeNeed->setLayoutDirection(Qt::LeftToRight);
        checkIsRecipeNeed->setStyleSheet(QStringLiteral("align: left;"));

        horizontalLayout_9->addWidget(checkIsRecipeNeed);

        horizontalSpacer_6 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_9->addItem(horizontalSpacer_6);

        layoutWidget7 = new QWidget(infAboutDrug);
        layoutWidget7->setObjectName(QStringLiteral("layoutWidget7"));
        layoutWidget7->setGeometry(QRect(10, 260, 291, 24));
        horizontalLayout_10 = new QHBoxLayout(layoutWidget7);
        horizontalLayout_10->setSpacing(6);
        horizontalLayout_10->setContentsMargins(11, 11, 11, 11);
        horizontalLayout_10->setObjectName(QStringLiteral("horizontalLayout_10"));
        horizontalLayout_10->setContentsMargins(0, 0, 0, 0);
        horizontalLayout_6 = new QHBoxLayout();
        horizontalLayout_6->setSpacing(6);
        horizontalLayout_6->setObjectName(QStringLiteral("horizontalLayout_6"));
        label_4 = new QLabel(layoutWidget7);
        label_4->setObjectName(QStringLiteral("label_4"));
        label_4->setMinimumSize(QSize(110, 0));

        horizontalLayout_6->addWidget(label_4);

        linePack = new QLineEdit(layoutWidget7);
        linePack->setObjectName(QStringLiteral("linePack"));
        linePack->setMinimumSize(QSize(0, 0));

        horizontalLayout_6->addWidget(linePack);


        horizontalLayout_10->addLayout(horizontalLayout_6);


        horizontalLayout_11->addWidget(infAboutDrug);


        verticalLayout_2->addLayout(horizontalLayout_11);

        horizontalLayout_4 = new QHBoxLayout();
        horizontalLayout_4->setSpacing(6);
        horizontalLayout_4->setObjectName(QStringLiteral("horizontalLayout_4"));
        horizontalSpacer = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_4->addItem(horizontalSpacer);

        btnCreate = new QPushButton(centralWidget);
        btnCreate->setObjectName(QStringLiteral("btnCreate"));

        horizontalLayout_4->addWidget(btnCreate);

        btnDelete = new QPushButton(centralWidget);
        btnDelete->setObjectName(QStringLiteral("btnDelete"));

        horizontalLayout_4->addWidget(btnDelete);

        btnFill = new QPushButton(centralWidget);
        btnFill->setObjectName(QStringLiteral("btnFill"));

        horizontalLayout_4->addWidget(btnFill);


        verticalLayout_2->addLayout(horizontalLayout_4);


        horizontalLayout_12->addLayout(verticalLayout_2);

        ClientClass->setCentralWidget(centralWidget);
        QWidget::setTabOrder(lineName, listCategory);
        QWidget::setTabOrder(listCategory, textEditPrescription);
        QWidget::setTabOrder(textEditPrescription, lineContraindication);
        QWidget::setTabOrder(lineContraindication, linePack);
        QWidget::setTabOrder(linePack, spinBoxCost);
        QWidget::setTabOrder(spinBoxCost, checkIsRecipeNeed);
        QWidget::setTabOrder(checkIsRecipeNeed, dateCertification);
        QWidget::setTabOrder(dateCertification, btnCreate);
        QWidget::setTabOrder(btnCreate, btnDelete);
        QWidget::setTabOrder(btnDelete, btnFill);
        QWidget::setTabOrder(btnFill, listRecords);

        retranslateUi(ClientClass);

        QMetaObject::connectSlotsByName(ClientClass);
    } // setupUi

    void retranslateUi(QMainWindow *ClientClass)
    {
        ClientClass->setWindowTitle(QApplication::translate("ClientClass", "\320\221\320\260\320\267\320\260 \320\264\320\260\320\275\320\275\321\213\321\205 \320\273\320\265\320\272\320\260\321\200\321\201\321\202\320\262\320\265\320\275\320\275\321\213\321\205 \321\201\321\200\320\265\320\264\321\201\321\202\320\262", 0));
        navNew->setText(QApplication::translate("ClientClass", "\320\241\320\276\320\267\320\264\320\260\321\202\321\214", 0));
        navOpen->setText(QApplication::translate("ClientClass", "\320\236\321\202\320\272\321\200\321\213\321\202\321\214", 0));
        navSave->setText(QApplication::translate("ClientClass", "\320\241\320\276\321\205\321\200\320\260\320\275\320\270\321\202\321\214", 0));
        navSaveAs->setText(QApplication::translate("ClientClass", "\320\241\320\276\321\205\321\200\320\260\320\275\320\270\321\202\321\214 \320\272\320\260\320\272...", 0));
        navExit->setText(QApplication::translate("ClientClass", "\320\222\321\213\321\205\320\276\320\264", 0));
        title->setText(QString());
        infAboutDrug->setTitle(QApplication::translate("ClientClass", "\320\224\320\260\320\275\320\275\321\213\320\265 \320\276 \320\273\320\265\320\272\320\260\321\200\321\201\321\202\320\262\320\265", 0));
        label->setText(QApplication::translate("ClientClass", "\320\235\320\260\320\267\320\262\320\260\320\275\320\270\320\265", 0));
        label_5->setText(QApplication::translate("ClientClass", "\320\241\321\202\320\276\320\270\320\274\320\276\321\201\321\202\321\214", 0));
        spinBoxCost->setSuffix(QApplication::translate("ClientClass", " \321\200\321\203\320\261.", 0));
        spinBoxCost->setPrefix(QString());
        label_2->setText(QApplication::translate("ClientClass", "\320\232\320\260\321\202\320\265\320\263\320\276\321\200\320\270\321\217", 0));

        const bool __sortingEnabled = listCategory->isSortingEnabled();
        listCategory->setSortingEnabled(false);
        QListWidgetItem *___qlistwidgetitem = listCategory->item(0);
        ___qlistwidgetitem->setText(QApplication::translate("ClientClass", "\320\266\320\260\321\200\320\276\320\277\320\276\320\275\320\270\320\266\320\260\321\216\321\211\320\265\320\265", 0));
        QListWidgetItem *___qlistwidgetitem1 = listCategory->item(1);
        ___qlistwidgetitem1->setText(QApplication::translate("ClientClass", "\320\260\320\275\321\202\320\270\320\261\320\270\320\276\321\202\320\270\320\272", 0));
        QListWidgetItem *___qlistwidgetitem2 = listCategory->item(2);
        ___qlistwidgetitem2->setText(QApplication::translate("ClientClass", "\320\260\320\275\320\260\320\273\321\214\320\263\320\265\321\202\320\270\320\272", 0));
        QListWidgetItem *___qlistwidgetitem3 = listCategory->item(3);
        ___qlistwidgetitem3->setText(QApplication::translate("ClientClass", "\320\221\320\220\320\224", 0));
        listCategory->setSortingEnabled(__sortingEnabled);

        label_6->setText(QApplication::translate("ClientClass", "\320\237\321\200\320\276\321\202\320\270\320\262\320\276\320\277\320\276\320\272\320\260\320\267\320\260\320\275\320\270\321\217", 0));
        label_8->setText(QApplication::translate("ClientClass", "\320\224\320\260\321\202\320\260 \321\201\320\265\321\200\321\202\320\270\321\204\320\270\320\272\320\260\321\206\320\270\320\270", 0));
        label_3->setText(QApplication::translate("ClientClass", "\320\235\320\260\320\267\320\275\320\260\321\207\320\265\320\275\320\270\320\265", 0));
        label_7->setText(QApplication::translate("ClientClass", "\320\242\321\200\320\265\320\261\321\203\320\265\321\202\321\201\321\217 \321\200\320\265\321\206\320\265\320\277\321\202", 0));
        checkIsRecipeNeed->setText(QString());
        label_4->setText(QApplication::translate("ClientClass", "\320\243\320\277\320\260\320\272\320\276\320\262\320\272\320\260", 0));
        btnCreate->setText(QApplication::translate("ClientClass", "\320\241\320\276\320\267\320\264\320\260\321\202\321\214", 0));
        btnDelete->setText(QApplication::translate("ClientClass", "\320\243\320\264\320\260\320\273\320\270\321\202\321\214", 0));
        btnFill->setText(QApplication::translate("ClientClass", "\320\227\320\260\320\277\320\276\320\273\320\275\320\270\321\202\321\214", 0));
    } // retranslateUi

};

namespace Ui {
    class ClientClass: public Ui_ClientClass {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_CLIENT_H
