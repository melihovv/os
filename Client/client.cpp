#include "client.h"

Client::Client(QWidget* parent)
    : QMainWindow(parent), currentPosition(-1), currentId(0), controller()
{
    ui.setupUi(this);

    QSettings settings(QCoreApplication::applicationDirPath() + "/config.ini", QSettings::IniFormat);
    QString serverPath = settings.value("client/serverPath", "server.exe").toString();

    if (controller.runServer(serverPath, "\\\\.\\pipe\\mypipe"))
    {
        checkServerTimer = new QTimer(this);
        checkServerTimer->start(3000);

        setValidation();
        setConnection();

        ui.title->setText("Название\t\t\t\t\t             Назначение");
        // Ограничить "Дату сертификации" сверху.
        ui.dateCertification->setMaximumDate(QDate::currentDate());

        loadDb();

        int count = controller.count();
        if (!count)
        {
            setActivity(false);
            ui.btnDelete->setEnabled(false);
        }
        else if (count == MAX_RECORDS)
        {
            ui.btnFill->setEnabled(false);
            ui.btnCreate->setEnabled(false);
        }
    }
    else
    {
        QMessageBox::critical(this, "Невозможно запустить сервер!", "Проверьте правильность указанного пути к серверу!");
        exit(1);
    }
}

Client::~Client()
{
    delete validators[0];
    delete validators[1];
    delete checkServerTimer;
}

void Client::setValidation()
{
    // Валидатор для поля "Название".
    validators[0] = new NameValidator(this);
    ui.lineName->setValidator(validators[0]);

    // Валидатор для поля "Упаковка".
    validators[1] = new PackValidator(this);
    ui.linePack->setValidator(validators[1]);
}

void Client::setConnection() const
{
    // Отключить активность "Нужен рецепт" и "Дата сертификации", если выбрано "БАД" в категории.
    connect(ui.listCategory, SIGNAL(currentRowChanged(int)), this, SLOT(disableIfBAD(int)));

    // Создать новую запись "по умолчанию" и вывести ее на экран при нажатии на кнопку "Создать".
    connect(ui.btnCreate, SIGNAL(clicked()), this, SLOT(createRecord()));

    // Создать десять различных записей при нажатии на кнопку "Заполнить".
    connect(ui.btnFill, SIGNAL(clicked()), this, SLOT(createRecords()));

    // Показать запись, выбранную в браузере.
    connect(ui.listRecords, SIGNAL(currentRowChanged(int)), this, SLOT(showRecordChoosedInBrowser(int)));

    // Удалить текущую запись при нажатии на кнопку "Удалить".
    connect(ui.btnDelete, SIGNAL(clicked()), this, SLOT(deleteCurRec()));

    // Сохранить данные в массив при потере виджетом фокуса.
    connect(qApp, SIGNAL(focusChanged(QWidget*, QWidget*)), this, SLOT(saveData(QWidget*, QWidget*)));

    // Проверка сервера каждую секунду.
    connect(checkServerTimer, SIGNAL(timeout()), this, SLOT(checkServer()));
}

void Client::setActivity(bool flag) const
{
    ui.infAboutDrug->setEnabled(flag);
}

void Client::setStandartWidgets() const
{
    ui.lineName->setText("");
    ui.listCategory->setCurrentRow(-1);
    ui.textEditPrescription->setPlainText("");
    ui.lineContraindication->setText("");
    ui.linePack->setText("");
    ui.spinBoxCost->setValue(0);
    ui.checkIsRecipeNeed->setChecked(false);
    ui.dateCertification->setDate(QDate(2000, 1, 1));
}

void Client::showRecord()
{
    showRecordWithoutBrowser();
    showRecordInBrowser();
}

void Client::showRecordWithoutBrowser()
{
    ui.lineName->setText(controller.record(currentId).getName());
    ui.listCategory->setCurrentRow(controller.record(currentId).getCategoryIndex());
    ui.textEditPrescription->setPlainText(controller.record(currentId).getPrescription());
    ui.lineContraindication->setText(controller.record(currentId).getContraindication());
    ui.linePack->setText(controller.record(currentId).getPack());
    ui.spinBoxCost->setValue(controller.record(currentId).getCost());
    ui.checkIsRecipeNeed->setChecked(controller.record(currentId).getIsRecipeNeed());
    ui.dateCertification->setDate(controller.record(currentId).getCertificationDate());
}

void Client::showRecordInBrowser()
{
    int amount = controller.count();
    RowOfBrowser rob(controller.record(currentId));

    // Запись создается.
    int a = ui.listRecords->count();
    if (ui.listRecords->count() < amount)
    {
        ui.listRecords->insertItem(currentPosition, rob.getRow());
        ui.listRecords->item(currentPosition)->setData(Qt::UserRole, currentId);
        ui.listRecords->setCurrentRow(currentPosition);
    }
    // Запись редактируется.
    else
    {
        ui.listRecords->currentItem()->setText(rob.getRow());
    }
}

void Client::loadDb()
{
    if (controller.count())
    {
        QVector<RowOfBrowser> recordsVector = controller.records();
        QVector<RowOfBrowser>::const_iterator iter;
        QVector<RowOfBrowser>::const_iterator vectorEnd = recordsVector.constEnd();
        iter = recordsVector.constBegin();

        int pos = 0;
        for (; iter != vectorEnd; ++iter)
        {
            ui.listRecords->addItem((*iter).getRow());
            ui.listRecords->item(pos++)->setData(Qt::UserRole, (*iter).getId());
        }

        if (!recordsVector.size())
        {
            ui.btnDelete->setEnabled(false);
            setActivity(false);
        }
        else
        {
            ui.btnDelete->setEnabled(true);
            setActivity(true);
            ui.listRecords->setCurrentRow(0);
        };
    }
}

void Client::disableIfBAD(int currentRow) const
{
    // Выбрано "БАД".
    if (currentRow == 3)
    {
        ui.checkIsRecipeNeed->setEnabled(false);
        ui.dateCertification->setEnabled(false);
    }
    else
    {
        ui.checkIsRecipeNeed->setEnabled(true);
        ui.dateCertification->setEnabled(true);
    }
}

void Client::showRecordChoosedInBrowser(int currentRow)
{
    if (currentRow >= 0)
    {
        currentPosition = currentRow;
        currentId = ui.listRecords->item(currentPosition)->data(Qt::UserRole).toInt();
        showRecordWithoutBrowser();
    }
    else
    {
        ui.listRecords->setCurrentRow(currentPosition);
    }
}

void Client::createRecord(Drug& another /* = Drug() */)
{
    int amount = controller.count();

    // Если создается первая запись активировать виджеты редактирования.
    if (!amount)
    {
        setActivity(true);
        ui.btnDelete->setEnabled(true);
    }
    // Если создается максимально возможная запись, то отключить кнопки "Создать" и "Заполнить".
    else if (amount == MAX_RECORDS - 1)
    {
        ui.btnCreate->setEnabled(false);
        ui.btnFill->setEnabled(false);
    }

    // Создать случайную запись или запись "по умолчанию".
    if (another == Drug())
    {
        Drug tempRec(0, "Парацетамол", (int) CategoryIndex::PYRETIC, "Грипп, насморк", "Хронический алкоголизм", "20 таблеток", 156, false, QDate(2000, 1, 1));

        // Вставить запись в подходящее место в массив.
        currentPosition = controller.append(tempRec);
        currentId = tempRec.getId();
    }
    // Создать запись на основе данной записи.
    else
    {
        currentPosition = controller.append(another);
        currentId = another.getId();
    }

    amount = controller.count();

    // Вывод записи в виждеты.
    showRecord();
}

void Client::createRecords()
{
    int amount = controller.count();
    int recordsCreated = 0;
    static Drug drugs[10] =
    {
        {0, "Парацетамол", (int) CategoryIndex::PYRETIC, "Грипп, насморк", "Хронический алкоголизм", "10 таблеток", 5, false, QDate(2000, 1, 1)},
        {0, "Аспирин", (int) CategoryIndex::PYRETIC, "Грипп, насморк", "Хронический алкоголизм", "10 таблеток", 135, true, QDate(2000, 1, 1)},
        {0, "Анальгин", (int) CategoryIndex::PYRETIC, "Грипп, насморк", "Хронический алкоголизм", "10 таблеток", 5, false, QDate(2000, 1, 1)},
        {0, "Азаран", (int) CategoryIndex::ANTIBIOTIC, "Перитонит, сепсис, менингит", "Повышенная чувствительность к антибиотикам группы цефалоспаринов", "10 таблеток", 3090, true, QDate(2000, 1, 1)},
        {0, "Азитрал", (int) CategoryIndex::ANTIBIOTIC, "Ангина, гайморит", "Повышенная чувствительность к препарату Азитрокс", "3 таблетки", 265, true, QDate(2000, 1, 1)},
        {0, "Азитрокс", (int) CategoryIndex::ANTIBIOTIC, "Тонзиллит, отит", "Повышенная чувствительность к препарату Азитрокс", "20 мл", 358, true, QDate(2000, 1, 1)},
        {0, "Вольтарен", (int) CategoryIndex::ANALGESIC, "Заболевания позвоночника, сопровождающиеся болевым синдромом", "Проктит, нарушения кроветворения", "20 штук", 318, false, QDate(2000, 1, 1)},
        {0, "Спазмалгон", (int) CategoryIndex::ANALGESIC, "Боли в суставах, артрит", "Выраженные нарушения функции печени или почек", "20 таблеток", 125, false, QDate(2000, 1, 1)},
        {0, "Экстракт гарцинии камбоджийской", (int) CategoryIndex::NUTRACEUTICALS, "Ожирение", "Не является лекарственным средством. Перед употреблением проконсультируйтесь с врачом", "20 таблеток", 1150, false, QDate(2000, 1, 1)},
        {0, "Молочнокислые бактерии", (int) CategoryIndex::NUTRACEUTICALS, "Заболевания кишечника", "Не является лекарственным средством. Перед употреблением проконсультируйтесь с врачом.", "20 таблеток", 1300, false, QDate(2000, 1, 1)}
    };

    for (int i = 0; i < 10 && amount + recordsCreated < MAX_RECORDS; ++i, ++recordsCreated)
    {
        createRecord(drugs[i]);
    }

}

void Client::deleteCurRec()
{
    if (ui.listRecords->currentRow() >= 0)
    {
        int amount = controller.count();

        // Удалить запись из массива.
        controller.remove(currentId);

        // Удаляется последняя запись.
        if (amount == 1)
        {
            ui.btnDelete->setEnabled(false);
            setActivity(false);
            setStandartWidgets();
        }
        // Удаляется запись, когда записей максимальное количество.
        else if (amount == MAX_RECORDS)
        {
            ui.btnCreate->setEnabled(true);
            ui.btnFill->setEnabled(true);
            ui.btnDelete->setEnabled(true);
        }

        // Уменьшить количество записей.
        amount = controller.count();

        // Удалить текущую запись из браузера.
        delete ui.listRecords->currentItem();

        if (amount == 0)
        {
            currentPosition = -1;
        }
        else
        {
            currentPosition = ui.listRecords->currentRow();
        }
    }
}

void Client::saveData(QWidget* last, QWidget* now)
{
    if (last != NULL && now != NULL)
    {
        QString lastObjectName = last->objectName();

        // Если изменяется "Назначение", то сохранить запись в массив и обновить браузер.
        if (lastObjectName == "textEditPrescription" &&
            ui.textEditPrescription->toPlainText() != controller.record(currentId).getPrescription())
        {
            int lineCount = ui.textEditPrescription->toPlainText().count('\n');
            // Количество строк равно нулю.
            if (lineCount < 1)
            {
                lineCount = 1;
            }

            QString str = ui.textEditPrescription->toPlainText();
            int length = str.length();

            bool isThereSymbolsExceptSpace = false;
            for (int i = 0; i < length && !isThereSymbolsExceptSpace; i++)
            {
                // Строка не пустая.
                if (!str[i].isSpace())
                {
                    isThereSymbolsExceptSpace = true;
                }
            }

            // Поле не заполнено.
            if (!isThereSymbolsExceptSpace)
            {
                ui.textEditPrescription->setPlainText(controller.record(currentId).getPrescription());
                QMessageBox::warning(this, "Корректность ввода", "Поле не должно быть пустым");
            }
            // Поле заполнено верно.
            else if (length <= 40 * lineCount)
            {
                controller.update(currentId, "Prescription", ui.textEditPrescription->toPlainText());
                showRecordInBrowser();
            }
            // В строке больше 40 символов.
            else
            {
                ui.textEditPrescription->setPlainText(controller.record(currentId).getPrescription());
                QMessageBox::warning(this, "Корректность ввода", "В поле должно быть не больше 40 символов");
            }
        }

        // Изменяются поля, от которых зависит сортировка.
        else if (lastObjectName == "lineName" || lastObjectName == "listCategory" || lastObjectName == "linePack")
        {
            int tempId = currentId;

            // Изменяется "Наименование".
            if (lastObjectName == "lineName")
            {
                // Поле заполнено правильно.
                if (ui.lineName->hasAcceptableInput())
                {
                    // И есть изменения.
                    if (ui.lineName->text() != controller.record(currentId).getName())
                    {
                        QString tempStr = ui.lineName->text();

                        delete ui.listRecords->takeItem(currentPosition);

                        currentId = tempId;
                        currentPosition = controller.update(currentId, "Name", tempStr);
                    }
                }
                // Поле заполнено неправильно.
                else
                {
                    // Поле пустое.
                    if (!ui.lineName->text().length())
                    {
                        QMessageBox::warning(this, "Корректность ввода", "Поле не должно быть пустым");
                    }
                    // Поле начинается не с заглавной буквы.
                    else
                    {
                        QMessageBox::warning(this, "Корректность ввода", "Поле должно начинаться с заглавной буквы");
                    }

                    ui.lineName->setText(controller.record(currentId).getName());
                }
            }

            // Изменяется "Категория".
            else if (lastObjectName == "listCategory" &&
                ui.listCategory->currentRow() != controller.record(currentId).getCategoryIndex())
            {
                int tempIndex = ui.listCategory->currentRow();

                delete ui.listRecords->takeItem(currentPosition);

                currentId = tempId;
                currentPosition = controller.update(currentId, "CategoryIndex", QString::number(tempIndex));
            }

            // Изменяется "Упаковка".
            else if (lastObjectName == "linePack")
            {
                // Поле заполнено правильно.
                if (ui.linePack->hasAcceptableInput())
                {
                    // И есть изменения.
                    if (ui.linePack->text() != controller.record(currentId).getPack())
                    {
                        QString tempStr = ui.linePack->text();

                        delete ui.listRecords->takeItem(currentPosition);

                        currentId = tempId;
                        currentPosition = controller.update(currentId, "Pack", tempStr);
                    }
                }
                // В поле нет цифры или буквы.
                else
                {
                    ui.linePack->setText(controller.record(currentId).getPack());
                    QMessageBox::warning(this, "Корректность ввода", "В строке должны быть цифры и буквы");
                }
            }

            showRecord();
        }

        // Если изменяются остальные виджеты.

        // Изменяется "Противопоказания".
        else if (lastObjectName == "lineContraindication" &&
            ui.lineContraindication->text() != controller.record(currentId).getContraindication())
        {
            controller.update(currentId, "Contraindication", ui.lineContraindication->text());
        }

        // Изменяется "Стоимость".
        else if (lastObjectName == "spinBoxCost" && ui.spinBoxCost->value() != controller.record(currentId).getCost())
        {
            controller.update(currentId, "Cost", QString::number(ui.spinBoxCost->value()));
        }

        // Изменяется "Требуется рецепт".
        else if (lastObjectName == "checkIsRecipeNeed" &&
            ui.checkIsRecipeNeed->isChecked() != controller.record(currentId).getIsRecipeNeed())
        {
            controller.update(currentId, "IsRecipeNeed", QString::number(ui.checkIsRecipeNeed->isChecked()));
        }

        // Изменяется "Дата сертификации".
        else if (lastObjectName == "dateCertification" &&
            ui.dateCertification->date() != controller.record(currentId).getCertificationDate())
        {
            controller.update(currentId, "CertificationDate", QString::number(ui.dateCertification->date().toJulianDay()));
        }
    }
}

void Client::closeEvent(QCloseEvent* e)
{
    controller.stopServer();
    e->accept();
}

void Client::checkServer()
{
    if (!controller.checkServer())
    {
        exit(1);
    }
}
