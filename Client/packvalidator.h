#ifndef PACKVALIDATOR_H
#define PACKVALIDATOR_H

#include <QValidator>
#include <QRegExp>

class PackValidator : public QValidator
{
    Q_OBJECT

public:
    PackValidator();
    PackValidator(QObject* parent);
    ~PackValidator();
    QValidator::State validate(QString& input, int& pos) const;
};

#endif // PACKVALIDATOR_H
