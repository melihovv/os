#include "drugmodel.h"

DrugModel::DrugModel(const QString& fileName) : maxId(0), isDbModified(false)
{
    this->fileName = fileName;
}

DrugModel::~DrugModel()
{
}

int DrugModel::count() const
{
    return data.size();
}

int DrugModel::append(Drug& rec)
{
    rec.setId(maxId++);

    data[rec.getId()] = rec;

    QMutableListIterator<uint> i(sortedIds);
    uint pos = 0;

    while (i.hasNext() && data[i.next()] <= rec)
    {
        ++pos;
    }

    if (!sortedIds.empty() && data[i.value()] > rec)
    {
        i.previous();
    }

    i.insert(rec.getId());
    isDbModified = true;

    return pos;
}

void DrugModel::remove(uint id)
{
    data.remove(id);
    sortedIds.removeOne(id);
    isDbModified = true;
}

int DrugModel::update(uint id, const QString& name, const QString& value)
{
    bool isUpdatePosNeed = false;
    isDbModified = true;

    if (name == "Name")
    {
        data[id].setName(value);
        isUpdatePosNeed = true;
    }
    else if (name == "CategoryIndex")
    {
        data[id].setCategoryIndex(value.toInt());
        isUpdatePosNeed = true;
    }
    else if (name == "Prescription")
    {
        data[id].setPrescription(value);
    }
    else if (name == "Contraindication")
    {
        data[id].setContraindication(value);
    }
    else if (name == "Pack")
    {
        data[id].setPack(value);
        isUpdatePosNeed = true;
    }
    else if (name == "Cost")
    {
        data[id].setCost(value.toInt());
    }
    else if (name == "IsRecipeNeed")
    {
        data[id].setIsRecipeNeed(value.toInt());
    }
    else if (name == "CertificationDate")
    {
        data[id].setCertificationDate(QDate::fromJulianDay(value.toInt()));
    }

    if (isUpdatePosNeed)
    {
        sortedIds.removeOne(id);
        QMutableListIterator<uint> i(sortedIds);
        uint pos = 0;

        while (i.hasNext() && data[i.next()] <= data[id])
        {
            ++pos;
        }

        if (!sortedIds.empty() && data[i.value()] > data[id])
        {
            i.previous();
        }

        i.insert(data[id].getId());

        return pos;
    }

    return sortedIds.indexOf(id);
}

const Drug DrugModel::record(uint id) const
{
    return data.value(id);
}

int DrugModel::records(RowOfBrowser* rob) const
{
    int count;
    QVector<RowOfBrowser> vector;
    QListIterator<uint> i(sortedIds);
    int amount = sortedIds.size();

    for (count = 0; count < amount; ++count)
    {
        RowOfBrowser lrob(data[i.next()]);
        rob[count] = lrob;
    }

    return count;
}

bool DrugModel::save() const
{
    LPCWSTR name = (LPCWSTR) fileName.utf16();
    HANDLE MyFile;

    MyFile = CreateFile(name, GENERIC_WRITE, FILE_SHARE_READ | FILE_SHARE_WRITE, NULL, CREATE_ALWAYS, FILE_ATTRIBUTE_NORMAL, NULL);

    if (MyFile != INVALID_HANDLE_VALUE)
    {
        DWORD bytesRead;
        int recCount = data.size();

        WriteFile(MyFile, (void*) &recCount, sizeof(int), &bytesRead, NULL);
        WriteFile(MyFile, (void*) &maxId, sizeof(uint), &bytesRead, NULL);

        QList<uint>::const_iterator listIter;
        listIter = sortedIds.constBegin();
        QList<uint>::const_iterator listEnd = sortedIds.constEnd();
        for (; listIter != listEnd; ++listIter)
        {
            uint id = *listIter;
            WriteFile(MyFile, (void*) &id, sizeof(uint), &bytesRead, NULL);
        }

        QHash<uint, Drug>::const_iterator hashIter;
        QHash<uint, Drug>::const_iterator hashEnd = data.constEnd();
        hashIter = data.constBegin();

        for (; hashIter != hashEnd; ++hashIter)
        {
            writeDrug(MyFile, *hashIter);
        }

        isDbModified = false;
        return true;
    }

    CloseHandle(MyFile);

    return false;
}

bool DrugModel::load()
{
    bool result = false;
    HANDLE myFile;
    LPCWSTR name = (LPCWSTR) fileName.utf16();

    myFile = CreateFile(name, GENERIC_READ, FILE_SHARE_READ | FILE_SHARE_WRITE, NULL, OPEN_ALWAYS, FILE_ATTRIBUTE_NORMAL, NULL);

    if (myFile != INVALID_HANDLE_VALUE)
    {
        DWORD bytesRead;
        uint id = 0;

        data.clear();
        sortedIds.clear();
        maxId = 0;

        int recCount = 0;
        ReadFile(myFile, (void*) &recCount, sizeof(int), &bytesRead, NULL);
        ReadFile(myFile, (void*) &maxId, sizeof(uint), &bytesRead, NULL);

        for (int i = 0; i < recCount; ++i)
        {
            ReadFile(myFile, (void*) &id, sizeof(uint), &bytesRead, NULL);
            sortedIds << id;
        }

        for (int i = 0; i < recCount; ++i)
        {
            Drug drug = readDrug(myFile);
            data[drug.getId()] = drug;
        }

        isDbModified = false;
        result = true;
    }

    CloseHandle(myFile);

    return result;
}

void DrugModel::clear()
{
    data.clear();
    sortedIds.clear();
    maxId = 0;
    isDbModified = true;
}

bool DrugModel::isModified() const
{
    return isDbModified == true;
}
