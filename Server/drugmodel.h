 #ifndef DRUGMODEL_H
#define DRUGMODEL_H

#define NOMINMAX

#include <windows.h>
#include <QHash>
#include <QList>
#include <QVector>
#include <QFile>
#include "../Common/drug.h"
#include "../Common/rowofbrowser.h"
#include "../Common/io.h"

class DrugModel
{
public:
    DrugModel(const QString& fileName);
    ~DrugModel();

    /*!
     * Получить количество записей.
     *\return Количество записей.
     */
    int count() const;

    /*!
     * Добавить запись в базу данных
     *\param[in|out] rec Запись лекарства.
     *\return Позиция записи в соотвествии с порядком сортировки запись с присвоенным ей id.
     */
    int append(Drug& rec);

    /*!
     * Удалить из базы данных запись с заданным идентификатором.
     *\param[in] id Идентификатор записи.
     */
    void remove(uint id);

    /*!
    * Изменить запись в базе данных.
    *\param[in] id Идентификатор редактируемой записи.
    *\param[in|out] name Название редактируемого поля.
    *\param[in|out] value Новое значение редактируемого поля (в текстовом формате).
    *\return Новая позиция записи в соответствии с порядком сортировки.
    */
    int update(uint id, const QString& name, const QString& value);

    /*!
     * Возвращает запись (только для чтения) по заданному идентификатору.
     *\param [in] id Идентификатор записи.
     */
    const Drug record(uint id) const;

    /*!
     * Получить все записи из бд для браузера.
     *\param[in|out] rob Указатель на строки браузера.
     *\return Количество записей.
     */
    int records(RowOfBrowser* rob) const;

    /*!
     * Сохранить данные в заданный файл.
     *\param[in] fileName Имя файла.
     *\return False, если сохранить данные не удалось, иначе - true.
     */
    bool save() const;

    /*!
     * Загрузить данные из заданного файла, при этом предыдущие данные уничтожаются.
     *\param[in] fileName Имя файла.
     *\return False, если сохранить данные не удалось, иначе - true.
     */
    bool load();

    /*!
     * Уничтожить все данные.
     */
    void clear();

    /*!
     * Показывает, имеются ли изменения БД после ее загрузки/сохранения.
     *\return Признак изменения базы данных.
     */
    bool isModified() const;

private:
    // Контейнер записей лекарств.
    QHash<uint, Drug> data;
    // Контейнер, хранящий id в отсортированном порядке.
    QList<uint> sortedIds;
    // Количество записей.
    uint maxId;
    // Изменилась ли бд.
    mutable bool isDbModified;
    // Имя файла бд.
    QString fileName;
};

#endif // DRUGMODEL_H
