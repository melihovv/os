#include <QCoreApplication>
#include <QSettings>
#include "spdlog/include/spdlog/spdlog.h"
#include "drugmodel.h"
#include "../Common/requesttype.h"
#include "../Common/io.h"

// TODO Избавиться от rob в records.
// TODO Избавиться от switch/case.
// TODO Сделать кэширование.

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);

    QSettings settings(QCoreApplication::applicationDirPath() + "/config.ini", QSettings::IniFormat);
    DrugModel model(settings.value("server/dbFile", "db.ddb").toString());
    QString requestsFileLog = settings.value("server/requestsFileLog", "requests.log").toString();
    QString errorsFileLog = settings.value("server/errorsFileLog", "requests.log").toString();
    QString pipeName = "\\\\.\\pipe\\mypipe";

#ifdef _DEBUG
    spdlog::set_level(spdlog::level::debug);
#else
    spdlog::set_level(spdlog::level::info);
#endif // _DEBUG

    auto console = spdlog::stdout_logger_mt("console");
    console->info("Server is running...");

    HANDLE hPipe = CreateNamedPipe(
        (const wchar_t*) pipeName.utf16(),
        PIPE_ACCESS_DUPLEX,
        PIPE_TYPE_MESSAGE | PIPE_READMODE_MESSAGE | PIPE_WAIT,
        254,
        1024,
        1024,
        NMPWAIT_WAIT_FOREVER,
        NULL);

    if (hPipe != INVALID_HANDLE_VALUE)
    {
        console->info("Named pipe is created. Waiting for clients connection...");

        ConnectNamedPipe(hPipe, NULL);
        console->info("Client is connected. Processing his requests...");

        RequestType requestType;
        DWORD bytes;

        model.load();

        while (true)
        {
            ReadFile(hPipe, (void*) &requestType, sizeof(int), &bytes, NULL);

            if (bytes)
            {
                switch ((int) requestType)
                {
                    case (int) RequestType::COUNT:
                    {
                        console->info("Request the number of records in db");

                        int size = model.count();
                        WriteFile(hPipe, (void*) &size, sizeof(int), &bytes, NULL);

                        break;
                    }
                    case (int) RequestType::APPEND:
                    {
                        console->info("Append a record in the db");

                        Drug drug = readDrug(hPipe);
                        int pos = model.append(drug);
                        uint id = drug.getId();

                        WriteFile(hPipe, (void*) &pos, sizeof(int), &bytes, NULL);
                        WriteFile(hPipe, (void*) &id, sizeof(uint), &bytes, NULL);

                        break;
                    }
                    case (int) RequestType::REMOVE:
                    {
                        console->info("Deleting a record from the db");

                        uint id;
                        ReadFile(hPipe, (void*) &id, sizeof(uint), &bytes, NULL);
                        model.remove(id);

                        break;
                    }
                    case (int) RequestType::UPDATE:
                    {
                        console->info("Refresh a record in the db");

                        uint id;
                        ReadFile(hPipe, (void*) &id, sizeof(uint), &bytes, NULL);

                        QString name = readString(hPipe);
                        QString value = readString(hPipe);

                        int pos = model.update(id, name, value);
                        WriteFile(hPipe, (void*) &pos, sizeof(int), &bytes, NULL);

                        break;
                    }
                    case (int) RequestType::RECORD:
                    {
                        console->info("Getting the record from the db by id");

                        uint id;
                        ReadFile(hPipe, (void*) &id, sizeof(uint), &bytes, NULL);

                        writeDrug(hPipe, model.record(id));

                        break;
                    }
                    case (int) RequestType::RECORDS:
                    {
                        console->info("Getting all records from the db");

                        int size = model.count();
                        WriteFile(hPipe, (void*) &size, sizeof(int), &bytes, NULL);

                        RowOfBrowser* rob = new RowOfBrowser[size];
                        model.records(rob);

                        for (int i = 0; i < size; ++i)
                        {
                            writeRowOfBrowser(hPipe, rob[i]);
                        }

                        delete[] rob;

                        break;
                    }
                    case (int) RequestType::SAVE:
                    {
                        console->info("Saving db");

                        model.save();

                        break;
                    }
                    case (int) RequestType::EXIT:
                    {
                        console->info("Shutdown");

                        model.save();
                        exit(0);

                        break;
                    }
                    case (int) RequestType::CHECK:
                    {
                        console->info("Check if server enable");

                        int response = 1;
                        WriteFile(hPipe, (void*) &response, sizeof(int), &bytes, NULL);

                        break;
                    }
                }
            }
        }

        DisconnectNamedPipe(hPipe);
    }
    else
    {
        console->error("Невозможно создать канал");
        exit(1);
    }

    return 0;
}
