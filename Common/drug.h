# ifndef DRUG_H
#define DRUG_H

#include <QString>
#include <QDate>
#include <QDataStream>

class Drug
{
public:
    Drug();
    Drug(uint id, const QString& name, int categoryIndex, const QString& prescription, const QString& contraindication, const QString& pack, int cost, bool isRecipeNeed, const QDate& certificationDate);

    bool operator==(const Drug& other) const;
    bool operator!=(const Drug& other) const;
    bool operator>(const Drug& other) const;
    bool operator<(const Drug& other) const;
    bool operator>=(const Drug& other) const;
    bool operator<=(const Drug& other) const;
    Drug& operator=(const Drug& other);
    friend QDataStream& operator<<(QDataStream& out, const Drug& rec);
    friend QDataStream& operator>>(QDataStream& in, Drug& rec);

    uint getId() const
    {
        return id;
    }
    void setId(uint val)
    {
        id = val;
    }

    QString getName() const
    {
        return name;
    }

    void setName(const QString& val)
    {
        name = val;
    }

    int getCategoryIndex() const
    {
        return categoryIndex;
    }

    void setCategoryIndex(int val)
    {
        categoryIndex = val;
    }

    QString getPrescription() const
    {
        return prescription;
    }

    void setPrescription(const QString& val)
    {
        prescription = val;
    }

    QString getContraindication() const
    {
        return contraindication;
    }
    void setContraindication(const QString& val)
    {
        contraindication = val;
    }

    QString getPack() const
    {
        return pack;
    }

    void setPack(const QString& val)
    {
        pack = val;
    }

    int getCost() const
    {
        return cost;
    }
    void setCost(int val)
    {
        cost = val;
    }

    bool getIsRecipeNeed() const
    {
        return isRecipeNeed;
    }
    void setIsRecipeNeed(bool val)
    {
        isRecipeNeed = val;
    }

    QDate getCertificationDate() const
    {
        return certificationDate;
    }

    void setCertificationDate(const QDate& val)
    {
        certificationDate = val;
    }

private:
    // Id записи.
    uint id;
    // Название.
    QString name;
    // Индекс значения категории в перечне.
    int categoryIndex;
    // Назначение.
    QString prescription;
    // Противопоказания.
    QString contraindication;
    // Упаковка.
    QString pack;
    // Стоимость.
    int cost;
    // Требуется ли рецепт.
    bool isRecipeNeed;
    // Дата сертификации.
    QDate certificationDate;
};

#endif // DRUG_H
