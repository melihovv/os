#ifndef REQUESTS_H
#define REQUESTS_H

enum class RequestType
{
    COUNT,
    APPEND,
    REMOVE,
    UPDATE,
    RECORD,
    RECORDS,
    SAVE,
    EXIT,
    CHECK
};

#endif // REQUESTS_H