#ifndef ROWOFBROWSER_H
#define ROWOFBROWSER_H

#include <QString>
#include <QStringList>
#include "../Common/drug.h"

class RowOfBrowser
{
public:
    RowOfBrowser();
    RowOfBrowser(const Drug& rec);
    ~RowOfBrowser();

    void makeStringForBrowser(const Drug& rec);

    uint getId() const
    {
        return id;
    }

    void setId(uint id)
    {
        this->id = id;
    }

    QString getRow() const
    {
        return row;
    }

    void setRow(const QString& val)
    {
        row = val;
    }

private:
    uint id;
    QString row;
};

#endif // ROWOFBROWSER_H
