#ifndef IO_H
#define IO_H

#define NOMINMAX
#include <windows.h>
#include <QString>
#include "drug.h"
#include "rowofbrowser.h"

/*!
 * Записать строку в файл
 *\param[in] file Дескриптор файла.
 *\param[in] str Строка.
 *\return Признак успешности записи строки.
 */
bool writeString(HANDLE file, const QString& str);

/*!
 * Прочитать строку из файла.
 *\param[in] file Дескриптор файла.
 *\return Прочитанная строка.
 */
QString readString(HANDLE file);

/*!
 * Записать запись лекарства в файл.
 *\param[in] file Дескриптор файла.
 *\param[in] drug Запись лекарства.
 *\return Признак успешности записи.
 */
bool writeDrug(HANDLE file, const Drug& drug);

/*!
 * Прочитать запись лекарства из файла.
 *\param[in] file Дескриптор файла.
 *\return Запись лекарства.
 */
Drug readDrug(HANDLE file);

/*!
 * Записать строку браузера.
 *\param[in] file Дескриптор файла.
 *\param[in] rob Строка браузера.
 *\return Признак успешности записи.
 */
bool writeRowOfBrowser(HANDLE file, const RowOfBrowser& rob);

/*!
 * Прочитать строку браузера из файла.
 *\param[in] file Дескриптор файла.
 *\return Прочитанная строка браузера.
 */
RowOfBrowser readRowOfBrowser(HANDLE file);

#endif // IO_H
