#include "rowofbrowser.h"

RowOfBrowser::RowOfBrowser() : id(0), row("")
{
}

RowOfBrowser::RowOfBrowser(const Drug& rec) : id(rec.getId())
{
    makeStringForBrowser(rec);
}

RowOfBrowser::~RowOfBrowser()
{
}

void RowOfBrowser::makeStringForBrowser(const Drug& rec)
{
    int lineCount = rec.getPrescription().count('\n');
    lineCount++;

    QStringList strList = rec.getPrescription().split('\n');
    row = rec.getName();

    int rowLength = row.length();

    if (rowLength < 12)
    {
        row += "\t\t\t\t";
    }
    else if (rowLength >= 12 && rowLength <= 25)
    {
        row += "\t\t\t";
    }
    else if (rowLength > 25 && rowLength < 40)
    {
        row += "\t\t";
    }
    else
    {
        row += "\t";
    }

    for (int i = 0; i < lineCount; i++)
    {
        // Строка не пустая.
        if (strList[i].length())
        {
            row += strList[i];
            bool stringIsNotEmpty = false;

            for (int j = i + 1; j < lineCount && !stringIsNotEmpty; j++)
            {
                // Строка не пустая.
                if (strList[j].length())
                {
                    row += ", ";
                    stringIsNotEmpty = true;
                }
            }
        }
    }
}
