#include "io.h"

bool writeString(HANDLE file, const QString& str)
{
    int specialStringSize = str.size() + 1;

    bool result;
    DWORD bytesRead;
    result = WriteFile(file, (void*) &specialStringSize, sizeof(int), &bytesRead, NULL);
    result &= WriteFile(file, (void*) str.data(), sizeof(QChar) * specialStringSize, &bytesRead, NULL);

    if (bytesRead != sizeof(QChar) * specialStringSize)
    {
        result = false;
    }

    return result;
}

QString readString(HANDLE file)
{
    DWORD bytesRead;
    int specialStringSize;
    QChar* specialString;
    QString str;

    ReadFile(file, (void*) &specialStringSize, sizeof(int), &bytesRead, NULL);

    specialStringSize--;
    str.resize(specialStringSize);

    specialStringSize++;
    ReadFile(file, (void*) str.data(), sizeof(QChar) * specialStringSize, &bytesRead, NULL);

    return str;
}

bool writeDrug(HANDLE file, const Drug& drug)
{
    DWORD bytesRead;
    LPVOID buffer;
    bool result;

    buffer = (void*) drug.getId();
    result = WriteFile(file, &buffer, sizeof(uint), &bytesRead, NULL);

    result &= writeString(file, drug.getName());

    buffer = (void*) drug.getCategoryIndex();
    result &= WriteFile(file, &buffer, sizeof(int), &bytesRead, NULL);

    result &= writeString(file, drug.getPrescription());
    result &= writeString(file, drug.getContraindication());
    result &= writeString(file, drug.getPack());

    buffer = (void*) drug.getCost();
    result &= WriteFile(file, &buffer, sizeof(int), &bytesRead, NULL);

    buffer = (void*) drug.getIsRecipeNeed();
    result &= WriteFile(file, &buffer, sizeof(bool), &bytesRead, NULL);

    result &= writeString(file, drug.getCertificationDate().toString());

    return result;
}

Drug readDrug(HANDLE file)
{
    DWORD bytesRead;
    Drug drug;

    uint id;
    ReadFile(file, &id, sizeof(uint), &bytesRead, NULL);
    drug.setId(id);
    drug.setName(readString(file));

    int categoryIndex;
    ReadFile(file, &categoryIndex, sizeof(int), &bytesRead, NULL);
    drug.setCategoryIndex(categoryIndex);

    drug.setPrescription(readString(file));
    drug.setContraindication(readString(file));
    drug.setPack(readString(file));

    int cost;
    ReadFile(file, &cost, sizeof(int), &bytesRead, NULL);
    drug.setCost(cost);

    bool isRecipeNeed;
    ReadFile(file, &isRecipeNeed, sizeof(bool), &bytesRead, NULL);

    drug.setIsRecipeNeed(isRecipeNeed);
    drug.setCertificationDate(QDate::fromString(readString(file)));

    return drug;
}

bool writeRowOfBrowser(HANDLE file, const RowOfBrowser& rob)
{
    DWORD bytesRead;

    LPVOID buffer = (void *) rob.getId();
    bool result = WriteFile(file, &buffer, sizeof(uint), &bytesRead, NULL);

    result &= writeString(file, rob.getRow());

    return result;
}

RowOfBrowser readRowOfBrowser(HANDLE file)
{
    DWORD bytesRead;

    uint id;
    ReadFile(file, &id, sizeof(uint), &bytesRead, NULL);

    RowOfBrowser rob;
    rob.setId(id);
    rob.setRow(readString(file));

    return rob;
}
