#include "drug.h"

Drug::Drug()
{
    id = 0;
    name = "";
    categoryIndex = -1;
    prescription = "";
    contraindication = "";
    pack = "";
    cost = 0;
    isRecipeNeed = false;
    certificationDate.setDate(2000, 1, 1);
}

Drug::Drug(uint id, const QString& name, int categoryIndex, const QString& prescription, const QString& contraindication, const QString& pack, int cost, bool isRecipeNeed, const QDate& certificationDate)
{
    this->id = id;
    this->name = name;
    this->categoryIndex = categoryIndex;
    this->prescription = prescription;
    this->contraindication = contraindication;
    this->pack = pack;
    this->cost = cost;
    this->isRecipeNeed = isRecipeNeed;
    this->certificationDate = certificationDate;
}

bool Drug::operator==(const Drug& other) const
{
    return name == other.name &&
        categoryIndex == other.categoryIndex &&
        prescription == other.prescription &&
        contraindication == other.contraindication &&
        pack == other.pack &&
        cost == other.cost &&
        isRecipeNeed == other.isRecipeNeed &&
        certificationDate == other.certificationDate;
}

bool Drug::operator!=(const Drug& other) const
{
    return !(*this == other);
}

bool Drug::operator>(const Drug& other) const
{
    if (categoryIndex > other.categoryIndex)
    {
        return true;
    }
    else if (categoryIndex == other.categoryIndex &&
        name > other.name)
    {
        return true;
    }
    else if (categoryIndex == other.categoryIndex &&
        name == other.name &&
        pack > other.pack)
    {
        return true;
    }

    return false;
}

bool Drug::operator<(const Drug& other) const
{
    return !(*this > other) && *this != other;
}

bool Drug::operator>=(const Drug& other) const
{
    return *this == other || *this > other;
}

bool Drug::operator<=(const Drug& other) const
{
    return *this == other || *this < other;
}

Drug& Drug::operator=(const Drug& other)
{
    if (this != &other)
    {
        id = other.id;
        name = other.name;
        categoryIndex = other.categoryIndex;
        prescription = other.prescription;
        contraindication = other.contraindication;
        pack = other.pack;
        cost = other.cost;
        isRecipeNeed = other.isRecipeNeed;
        certificationDate = other.certificationDate;
    }

    return *this;
}

QDataStream& operator<<(QDataStream& out, const Drug& rec)
{
    out << rec.id
        << rec.name
        << rec.categoryIndex
        << rec.prescription
        << rec.contraindication
        << rec.pack
        << rec.cost
        << rec.isRecipeNeed
        << rec.certificationDate;

    return out;
}

QDataStream& operator>>(QDataStream& in, Drug& rec)
{
    in >> rec.id
        >> rec.name
        >> rec.categoryIndex
        >> rec.prescription
        >> rec.contraindication
        >> rec.pack
        >> rec.cost
        >> rec.isRecipeNeed
        >> rec.certificationDate;

    return in;
}
